package ru.malyshev.gamelibrary.services.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.Publisher;
import ru.malyshev.gamelibrary.repositories.GamesRepository;
import ru.malyshev.gamelibrary.repositories.PublishersRepository;
import ru.malyshev.gamelibrary.repositories.UsersRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class GamesServiceImplTest {

    private static final long EXISTED_PUBLISHER_ID = 1L;
    private static final long NOT_EXISTED_PUBLISHER_ID = 2L;

    private static final Publisher EXISTED_PUBLISHER = Publisher.builder()
            .id(EXISTED_PUBLISHER_ID)
            .name("Publisher1")
            .state(Publisher.State.NOT_READY)
            .build();

    private static final long EXISTED_GAME_ID_1 = 1L;
    private static final long EXISTED_GAME_ID_2 = 2L;
    private static final long NOT_EXISTED_GAME_ID = 3L;

    private static final Game EXISTED_GAME_1 = Game.builder()
            .id(EXISTED_GAME_ID_1)
            .title("Game1")
            .state(Game.State.NOT_READY)
            .build();

    private static final Game EXISTED_GAME_2 = Game.builder()
            .id(EXISTED_GAME_ID_1)
            .title("Game2")
            .state(Game.State.NOT_READY)
            .build();

    private GamesServiceImpl gamesService;

    private GamesRepository gamesRepository;

    private PublishersRepository publishersRepository;

    private UsersRepository usersRepository;

    @BeforeEach
    void setUp() {
        setUpMocks();
        stubMocks();
        this.gamesService = new GamesServiceImpl(gamesRepository, usersRepository, publishersRepository);
    }

    void setUpMocks() {
        this.gamesRepository = Mockito.mock(GamesRepository.class);
        this.publishersRepository = Mockito.mock(PublishersRepository.class);
        this.usersRepository = Mockito.mock(UsersRepository.class);
    }

    void stubMocks() {
        when(gamesRepository.findAllByPublisher_IdAndStateNot(EXISTED_PUBLISHER_ID, Game.State.DELETED)).thenReturn(List.of(EXISTED_GAME_1));

        when(publishersRepository.findById(EXISTED_PUBLISHER_ID)).thenReturn(Optional.of(EXISTED_PUBLISHER));
        when(publishersRepository.findById(NOT_EXISTED_PUBLISHER_ID)).thenReturn(Optional.empty());

        when(gamesRepository.findById(EXISTED_GAME_ID_2)).thenReturn(Optional.of(EXISTED_GAME_2));
        when(gamesRepository.findById(NOT_EXISTED_GAME_ID)).thenReturn(Optional.empty());

    }

    @Test
    void addPublisherToGame() {
        gamesService.addPublisherToGame(EXISTED_PUBLISHER_ID, EXISTED_GAME_ID_2);
        verify(gamesRepository).save(EXISTED_GAME_2);
    }

    @Test
    void addPublisherToGameForNotExistedGame() {
        assertThrows(RuntimeException.class, () -> gamesService.addPublisherToGame(EXISTED_PUBLISHER_ID, NOT_EXISTED_GAME_ID));
    }

    @Test
    void addPublisherToGameForNotExistedPublisher() {
        assertThrows(RuntimeException.class, () -> gamesService.addPublisherToGame(NOT_EXISTED_PUBLISHER_ID, EXISTED_GAME_ID_2));
    }

    @Test
    void getAllGamesByPublisher() {
        List<Game> expected = List.of(EXISTED_GAME_1);
        List<Game> actual = gamesService.getAllGamesByPublisher(1L);

        assertArrayEquals(expected.toArray(), actual.toArray());
    }
}