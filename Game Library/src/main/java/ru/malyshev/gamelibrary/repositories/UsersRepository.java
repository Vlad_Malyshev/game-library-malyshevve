package ru.malyshev.gamelibrary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateOrderById(User.State state);

    List<User> findAllByStateNotOrderById(User.State state);

    List<User> findAllByFavoritesContainsAndStateOrderById(Game game, User.State state);

    Optional<User> findByEmail(String email);
}
