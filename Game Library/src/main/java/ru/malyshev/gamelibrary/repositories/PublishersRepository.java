package ru.malyshev.gamelibrary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malyshev.gamelibrary.models.Publisher;

import java.util.List;

public interface PublishersRepository extends JpaRepository<Publisher, Long> {
    List<Publisher> findAllByStateNotOrderById(Publisher.State state);

    List<Publisher> findAllByStateOrderById(Publisher.State state);
}
