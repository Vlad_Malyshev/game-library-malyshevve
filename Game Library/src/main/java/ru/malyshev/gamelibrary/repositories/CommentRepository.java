package ru.malyshev.gamelibrary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malyshev.gamelibrary.models.Comment;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByGame_IdAndState(Long gameId, Comment.State state);

    List<Comment> findAllByUser_IdAndState(Long userId, Comment.State state);
}
