package ru.malyshev.gamelibrary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.User;

import java.util.List;

public interface GamesRepository extends JpaRepository<Game, Long> {
    List<Game> findAllByStateNotOrderById(Game.State state);

    List<Game> findAllByStateNotAndTitleContainsOrderById(Game.State state, String titleLike);

    List<Game> findAllByStateAndAgeLimitLessThanEqualOrderById(Game.State state, Integer age);

    List<Game> findAllByStateAndAgeLimitLessThanEqualAndTitleContainsOrderById(Game.State state, Integer age, String titleLike);

    List<Game> findAllByUsersContainsAndStateOrderByTitle(User user, Game.State state);

    List<Game> findAllByStateAndAgeLimitLessThanEqualAndPublisher_Id(Game.State state, Integer age, Long publisherId);

    List<Game> findAllByPublisher_IdAndStateNot(Long publisherId, Game.State state);
}
