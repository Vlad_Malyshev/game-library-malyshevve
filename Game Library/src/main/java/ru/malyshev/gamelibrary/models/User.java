package ru.malyshev.gamelibrary.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Check;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"favorites"})
@ToString(exclude = {"favorites"})
@Entity
@Table(name = "account")
public class User {

    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }

    public enum Role {
        USER, ADMIN
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Check(constraints = "age >= 0 and age <= 120")
    @Builder.Default
    private Integer age = 0;

    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "game_id", referencedColumnName = "id")})
    private Set<Game> favorites;

    @Enumerated(value = EnumType.STRING)
    @Builder.Default
    private State state = State.NOT_CONFIRMED;

    @Enumerated(value = EnumType.STRING)
    @Builder.Default
    private Role role = Role.USER;
}
