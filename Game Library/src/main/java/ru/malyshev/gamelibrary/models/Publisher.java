package ru.malyshev.gamelibrary.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = "games")
@ToString(exclude = "games")
@Entity
public class Publisher {

    public enum State {
        READY, NOT_READY, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Builder.Default
    private String country = "пусто";

    @OneToMany(mappedBy = "publisher", fetch = FetchType.EAGER)
    private Set<Game> games;

    @Enumerated(value = EnumType.STRING)
    @Builder.Default
    private State state = State.NOT_READY;
}
