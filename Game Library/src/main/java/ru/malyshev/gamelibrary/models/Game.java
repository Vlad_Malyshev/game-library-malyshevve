package ru.malyshev.gamelibrary.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"publisher", "users"})
@Entity
public class Game {

    public enum State {
        READY, NOT_READY, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 5000)
    @Builder.Default
    private String description = "Обычная игра";

    @Builder.Default
    private String genre = "псуто";

    @Builder.Default
    private LocalDate releaseDate = LocalDate.of(1, 1, 1);

    @Column(name = "interface_language")
    @Builder.Default
    private String interfaceLanguage = "пусто";

    @Column(name = "voice_language")
    @Builder.Default
    private String voiceLanguage = "пусто";

    @Column(name = "age_limit")
    @Builder.Default
    private Integer ageLimit = 0;

    @Column(name = "image_url")
    @Builder.Default
    private String imageUrl = "пусто";

    @Column(name = "video_code")
    @Builder.Default
    private String videoCode = "пусто";

    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;

    @ManyToMany(mappedBy = "favorites", fetch = FetchType.EAGER)
    private Set<User> users;

    @Enumerated(value = EnumType.STRING)
    @Builder.Default
    private State state = State.NOT_READY;
}
