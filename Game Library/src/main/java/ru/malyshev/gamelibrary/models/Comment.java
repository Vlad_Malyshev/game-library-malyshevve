package ru.malyshev.gamelibrary.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"user", "game"})
@Entity
public class Comment {

    public enum State {
        DELETED, PUBLISHED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(length = 5000)
    private String comment;

    private LocalDate date;

    @Enumerated(value = EnumType.STRING)
    @Builder.Default
    private State state = State.PUBLISHED;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;
}
