package ru.malyshev.gamelibrary.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GameForm {
    private String title;

    private String description;
    private String genre;
    private LocalDate releaseDate;
    private String interfaceLanguage;
    private String voiceLanguage;
    private Integer ageLimit;
    private String imageUrl;
    private String videoCode;
}
