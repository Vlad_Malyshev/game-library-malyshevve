package ru.malyshev.gamelibrary.services;

import ru.malyshev.gamelibrary.dto.GameForm;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.User;

import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import java.util.List;

public interface GamesService {
    Game getGame(Long id);

    List<Game> getAllGames();

    List<Game> getAllReadyAndAgeLimitLessThanAgeUserGames(Integer userAge);

    void addGame(GameForm game);

    void deleteGame(Long gameId);

    void prepareGame(Long gameId);

    void unprepareGame(Long gameId);

    void updateGame(Long gameId, GameForm game);

    void addPublisherToGame(Long publisherId, Long gameId);

    List<Game> getAllGamesByPublisher(Long publisherId);

    List<User> getAllUsersWhoLikeIt(Long gameId);

    List<Game> getAllByPublisherReadyAndAgeLimitLessThanAgeUserGames(Integer age, Long publisherId);

    void addPhoto(Long gameId, GameForm game);

    void addVideo(Long gameId, GameForm game);

    List<Game> getAllGamesTitleLike(String titleLike);

    List<Game> getAllTitleLikeReadyAndAgeLimitLessThanAgeUserGames(Integer age, String titleLike);
}
