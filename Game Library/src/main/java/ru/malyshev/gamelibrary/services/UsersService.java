package ru.malyshev.gamelibrary.services;

import ru.malyshev.gamelibrary.dto.ConfirmUserForm;
import ru.malyshev.gamelibrary.dto.UserForm;
import ru.malyshev.gamelibrary.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllReadyUsers();

    List<User> getAllUsers();

    User getUser(Long userId);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);

    void giveRoleAdmin(Long userId);

    Boolean confirmUserByEmail(String userEmail, String confirmCodeEncoded, ConfirmUserForm confirmUserForm);

    void confirmUserByAge(String userEmail, UserForm userForm);
}
