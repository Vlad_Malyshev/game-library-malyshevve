package ru.malyshev.gamelibrary.services;

import ru.malyshev.gamelibrary.dto.UserForm;

public interface SignUpService {
    String signUp(UserForm userForm);
}
