package ru.malyshev.gamelibrary.services;

public interface EmailService {
    String sendConfirmMessage(String toAddress);
}
