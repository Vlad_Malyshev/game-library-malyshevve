package ru.malyshev.gamelibrary.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.repositories.GamesRepository;
import ru.malyshev.gamelibrary.repositories.UsersRepository;
import ru.malyshev.gamelibrary.security.details.CustomUserDetails;
import ru.malyshev.gamelibrary.services.EmailService;
import ru.malyshev.gamelibrary.services.ProfileService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;

    private final GamesRepository gamesRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }

    @Override
    public void changeUserPassword(CustomUserDetails userDetails, String newPassword) {
        User user = usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
        user.setPassword(passwordEncoder.encode(newPassword));
        usersRepository.save(user);
    }

    @Override
    public void addFavoriteGame(CustomUserDetails customUserDetails, Long gameId) {
        User user = usersRepository.findById(customUserDetails.getUser().getId()).orElseThrow();
        user.getFavorites().add(gamesRepository.findById(gameId).orElseThrow());
        usersRepository.save(user);
    }

    @Override
    public void removeFavoriteGame(CustomUserDetails customUserDetails, Long gameId) {
        User user = usersRepository.findById(customUserDetails.getUser().getId()).orElseThrow();
        user.getFavorites().remove(gamesRepository.findById(gameId).orElseThrow());
        usersRepository.save(user);
    }

    @Override
    public List<Game> getFavoritesGames(CustomUserDetails userDetails) {
        User user = usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
        return gamesRepository.findAllByUsersContainsAndStateOrderByTitle(user, Game.State.READY);
    }
}
