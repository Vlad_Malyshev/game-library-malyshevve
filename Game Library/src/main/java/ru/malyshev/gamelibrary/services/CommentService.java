package ru.malyshev.gamelibrary.services;

import ru.malyshev.gamelibrary.dto.CommentForm;
import ru.malyshev.gamelibrary.models.Comment;
import ru.malyshev.gamelibrary.models.User;

import java.util.List;

public interface CommentService {

    List<Comment> getAllCommentToGame(Long gameId);

    void addComment(Long gameId, User user, CommentForm commentForm);


    void deleteComment(Long commentId);

    List<Comment> getAllCommentByUser(Long userId);
}
