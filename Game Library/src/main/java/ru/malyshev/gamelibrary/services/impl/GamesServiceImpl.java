package ru.malyshev.gamelibrary.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.malyshev.gamelibrary.dto.GameForm;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.Publisher;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.repositories.GamesRepository;
import ru.malyshev.gamelibrary.repositories.PublishersRepository;
import ru.malyshev.gamelibrary.repositories.UsersRepository;
import ru.malyshev.gamelibrary.services.GamesService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GamesServiceImpl implements GamesService {

    private final GamesRepository gamesRepository;

    private final UsersRepository usersRepository;

    private final PublishersRepository publishersRepository;

    @Override
    public Game getGame(Long id) {
        return gamesRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Game> getAllGames() {
        return gamesRepository.findAllByStateNotOrderById(Game.State.DELETED);
    }

    @Override
    public List<Game> getAllReadyAndAgeLimitLessThanAgeUserGames(Integer userAge) {
        return gamesRepository.findAllByStateAndAgeLimitLessThanEqualOrderById(Game.State.READY, userAge);
    }

    @Override
    public List<Game> getAllGamesByPublisher(Long publisherId) {
        return gamesRepository.findAllByPublisher_IdAndStateNot(publisherId, Game.State.DELETED);
    }

    @Override
    public List<User> getAllUsersWhoLikeIt(Long gameId) {
        Game game = gamesRepository.findById(gameId).orElseThrow();
        return usersRepository.findAllByFavoritesContainsAndStateOrderById(game, User.State.CONFIRMED);
    }

    @Override
    public List<Game> getAllByPublisherReadyAndAgeLimitLessThanAgeUserGames(Integer age, Long publisherId) {
        return gamesRepository.findAllByStateAndAgeLimitLessThanEqualAndPublisher_Id(Game.State.READY, age, publisherId);
    }

    @Override
    public List<Game> getAllGamesTitleLike(String titleLike) {
        return gamesRepository.findAllByStateNotAndTitleContainsOrderById(Game.State.DELETED, titleLike);
    }

    @Override
    public List<Game> getAllTitleLikeReadyAndAgeLimitLessThanAgeUserGames(Integer age, String titleLike) {
        return gamesRepository.findAllByStateAndAgeLimitLessThanEqualAndTitleContainsOrderById(Game.State.READY, age, titleLike);
    }

    @Override
    public void addGame(GameForm game) {
        Game newGame = Game.builder()
                .title(game.getTitle())
                .build();
        gamesRepository.save(newGame);
    }

    @Override
    public void deleteGame(Long id) {
        Game game = gamesRepository.findById(id).orElseThrow();
        game.setState(Game.State.DELETED);
        gamesRepository.save(game);
    }

    @Override
    public void prepareGame(Long gameId) {
        Game game = gamesRepository.findById(gameId).orElseThrow();
        game.setState(Game.State.READY);
        gamesRepository.save(game);
    }

    @Override
    public void unprepareGame(Long gameId) {
        Game game = gamesRepository.findById(gameId).orElseThrow();
        game.setState(Game.State.NOT_READY);
        gamesRepository.save(game);
    }

    @Override
    public void updateGame(Long courseId, GameForm updateData) {
        Game gameForUpdate = gamesRepository.findById(courseId).orElseThrow();
        gameForUpdate.setDescription(updateData.getDescription());
        gameForUpdate.setGenre(updateData.getGenre());
        gameForUpdate.setReleaseDate(updateData.getReleaseDate());
        gameForUpdate.setInterfaceLanguage(updateData.getInterfaceLanguage());
        gameForUpdate.setVoiceLanguage(updateData.getVoiceLanguage());
        gameForUpdate.setAgeLimit(updateData.getAgeLimit());
        gamesRepository.save(gameForUpdate);
    }

    @Override
    public void addPublisherToGame(Long publisherId, Long gameId) {
        Publisher publisher = publishersRepository.findById(publisherId).orElseThrow();
        Game game = gamesRepository.findById(gameId).orElseThrow();
        game.setPublisher(publisher);
        gamesRepository.save(game);
    }

    @Override
    public void addPhoto(Long gameId, GameForm game) {
        Game gameForAdd = gamesRepository.findById(gameId).orElseThrow();
        gameForAdd.setImageUrl(game.getImageUrl());
        gamesRepository.save(gameForAdd);
    }

    @Override
    public void addVideo(Long gameId, GameForm game) {
        Game gameForAdd = gamesRepository.findById(gameId).orElseThrow();
        gameForAdd.setVideoCode(game.getVideoCode());
        gamesRepository.save(gameForAdd);
    }
}
