package ru.malyshev.gamelibrary.services;

import ru.malyshev.gamelibrary.dto.PublisherForm;
import ru.malyshev.gamelibrary.models.Publisher;

import java.util.List;

public interface PublishersService {

    List<Publisher> getAllPublishers();

    List<Publisher> getAllReadyPublishers();

    void addPublisher(PublisherForm publisher);

    void deletePublisher(Long publisherId);

    void preparePublisher(Long publisherId);

    void unpreparePublisher(Long publisherId);

    Publisher getPublisher(Long publisherId);

    void updatePublisher(Long publisherId, PublisherForm publisher);
}
