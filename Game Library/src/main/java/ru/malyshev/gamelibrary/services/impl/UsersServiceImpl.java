package ru.malyshev.gamelibrary.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.malyshev.gamelibrary.dto.ConfirmUserForm;
import ru.malyshev.gamelibrary.dto.UserForm;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.repositories.UsersRepository;
import ru.malyshev.gamelibrary.services.UsersService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAllReadyUsers() {
        return usersRepository.findAllByStateOrderById(User.State.CONFIRMED);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAllByStateNotOrderById(User.State.DELETED);
    }

    @Override
    public User getUser(Long userId) {
        return usersRepository.findById(userId).orElseThrow();
    }

    @Override
    public void updateUser(Long userId, UserForm updateData) {
        User userForUpdate = usersRepository.findById(userId).orElseThrow();

        userForUpdate.setFirstName(updateData.getFirstName());
        userForUpdate.setLastName(updateData.getLastName());
        userForUpdate.setAge(updateData.getAge());

        usersRepository.save(userForUpdate);
    }

    @Override
    public void deleteUser(Long userId) {
        User userForDelete = usersRepository.findById(userId).orElseThrow();
        userForDelete.setState(User.State.DELETED);
        usersRepository.save(userForDelete);
    }

    @Override
    public void giveRoleAdmin(Long userId) {
        User user = usersRepository.findById(userId).orElseThrow();
        user.setRole(User.Role.ADMIN);
        usersRepository.save(user);
    }

    @Override
    public Boolean confirmUserByEmail(String userEmail, String confirmCodeEncoded, ConfirmUserForm confirmUserForm) {
        User user = usersRepository.findByEmail(userEmail).orElseThrow();
        Integer userConfirmCode = confirmUserForm.getConfirmCode();
        if (passwordEncoder.matches(userConfirmCode.toString(), confirmCodeEncoded)) {
            user.setState(User.State.CONFIRMED);
            usersRepository.save(user);
        }
        return passwordEncoder.matches(userConfirmCode.toString(), confirmCodeEncoded);
    }

    @Override
    public void confirmUserByAge(String userEmail, UserForm userForm) {
        User user = usersRepository.findByEmail(userEmail).orElseThrow();
        user.setAge(userForm.getAge());
        usersRepository.save(user);
    }


}
