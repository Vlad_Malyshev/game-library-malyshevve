package ru.malyshev.gamelibrary.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.malyshev.gamelibrary.dto.CommentForm;
import ru.malyshev.gamelibrary.models.Comment;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.repositories.CommentRepository;
import ru.malyshev.gamelibrary.repositories.GamesRepository;
import ru.malyshev.gamelibrary.services.CommentService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    private final GamesRepository gamesRepository;

    @Override
    public List<Comment> getAllCommentToGame(Long gameId) {
        return commentRepository.findAllByGame_IdAndState(gameId, Comment.State.PUBLISHED);
    }

    @Override
    public void addComment(Long gameId, User user, CommentForm commentForm) {
        if(commentForm.getComment().length() > 0) {
            Game game = gamesRepository.findById(gameId).orElseThrow();
            Comment comment = Comment.builder()
                    .comment(commentForm.getComment())
                    .date(LocalDate.now())
                    .user(user)
                    .game(game)
                    .build();
            commentRepository.save(comment);
        }
    }

    @Override
    public void deleteComment(Long commentId) {
        Comment comment = commentRepository.findById(commentId).orElseThrow();
        comment.setState(Comment.State.DELETED);
        commentRepository.save(comment);
    }

    @Override
    public List<Comment> getAllCommentByUser(Long userId) {
        return commentRepository.findAllByUser_IdAndState(userId, Comment.State.PUBLISHED);
    }
}
