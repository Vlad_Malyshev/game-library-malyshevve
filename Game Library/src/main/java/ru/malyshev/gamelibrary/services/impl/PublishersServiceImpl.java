package ru.malyshev.gamelibrary.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.malyshev.gamelibrary.dto.PublisherForm;
import ru.malyshev.gamelibrary.models.Publisher;
import ru.malyshev.gamelibrary.repositories.PublishersRepository;
import ru.malyshev.gamelibrary.services.PublishersService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PublishersServiceImpl implements PublishersService {

    private final PublishersRepository publishersRepository;

    @Override
    public List<Publisher> getAllPublishers() {
        return publishersRepository.findAllByStateNotOrderById(Publisher.State.DELETED);
    }

    @Override
    public List<Publisher> getAllReadyPublishers() {
        return publishersRepository.findAllByStateOrderById(Publisher.State.READY);
    }

    @Override
    public void addPublisher(PublisherForm publisher) {
        Publisher newLesson = Publisher.builder()
                .name(publisher.getName())
                .build();
        publishersRepository.save(newLesson);
    }

    @Override
    public void deletePublisher(Long publisherId) {
        Publisher publisher = publishersRepository.findById(publisherId).orElseThrow();
        publisher.setState(Publisher.State.DELETED);
        publishersRepository.save(publisher);
    }

    @Override
    public void preparePublisher(Long publisherId) {
        Publisher publisher = publishersRepository.findById(publisherId).orElseThrow();
        publisher.setState(Publisher.State.READY);
        publishersRepository.save(publisher);
    }

    @Override
    public void unpreparePublisher(Long publisherId) {
        Publisher publisher = publishersRepository.findById(publisherId).orElseThrow();
        publisher.setState(Publisher.State.NOT_READY);
        publishersRepository.save(publisher);
    }

    @Override
    public Publisher getPublisher(Long publisherId) {
        return publishersRepository.findById(publisherId).orElseThrow();
    }

    @Override
    public void updatePublisher(Long publisherId, PublisherForm updateData) {
        Publisher publisherForUpdate = publishersRepository.findById(publisherId).orElseThrow();
        publisherForUpdate.setCountry(updateData.getCountry());
        publishersRepository.save(publisherForUpdate);
    }
}
