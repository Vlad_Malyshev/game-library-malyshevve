package ru.malyshev.gamelibrary.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.malyshev.gamelibrary.dto.UserForm;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.repositories.UsersRepository;
import ru.malyshev.gamelibrary.services.SignUpService;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final UsersRepository usersRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public String signUp(UserForm userForm) {
        User user = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .email(userForm.getEmail())
                .password(passwordEncoder.encode(userForm.getPassword()))
                .build();
        usersRepository.save(user);
        return userForm.getEmail();
    }
}
