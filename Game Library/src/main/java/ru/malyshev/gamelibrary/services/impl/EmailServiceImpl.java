package ru.malyshev.gamelibrary.services.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.malyshev.gamelibrary.services.EmailService;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    private final PasswordEncoder passwordEncoder;

    @Override
    public String sendConfirmMessage(String toAddress){

        int confirmCode = RandomUtils.nextInt(100_000, 999_999);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("gamelybrarybymalyshevve@gmail.com");
        mailMessage.setTo(toAddress);
        mailMessage.setSubject("Добро пожаловать!");
        mailMessage.setText("Ваш код подтверждения: " + confirmCode);

        javaMailSender.send(mailMessage);

        String confirmCodeEncoded = passwordEncoder.encode(Integer.toString(confirmCode));

        while (confirmCodeEncoded.contains("/")) {
            confirmCodeEncoded = passwordEncoder.encode(Integer.toString(confirmCode));
        }

        return confirmCodeEncoded;
    }
}
