package ru.malyshev.gamelibrary.services;

import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.security.details.CustomUserDetails;

import java.util.List;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);

    void changeUserPassword(CustomUserDetails userDetails, String newPassword);

    void addFavoriteGame(CustomUserDetails customUserDetails, Long gameId);

    List<Game> getFavoritesGames(CustomUserDetails userDetails);

    void removeFavoriteGame(CustomUserDetails customUserDetails, Long gameId);
}
