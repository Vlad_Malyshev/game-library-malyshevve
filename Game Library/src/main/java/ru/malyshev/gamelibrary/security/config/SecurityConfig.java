package ru.malyshev.gamelibrary.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
public class SecurityConfig {

    @Autowired
    private UserDetailsService customUserDetailsService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity, PersistentTokenRepository tokenRepository) throws Exception {
        httpSecurity.csrf().disable();

        httpSecurity.formLogin()
                .loginPage("/signIn")
                .defaultSuccessUrl("/games")
                .failureUrl("/signIn?error")
                .usernameParameter("email")
                .passwordParameter("password")
                .permitAll();

        httpSecurity.rememberMe()
                .rememberMeParameter("rememberMe")
                .tokenRepository(tokenRepository)
                .tokenValiditySeconds(60 * 60 * 24 * 365);

        httpSecurity.authorizeHttpRequests().requestMatchers("/css/**").permitAll();
        httpSecurity.authorizeHttpRequests().requestMatchers("/signIn/**").permitAll();
        httpSecurity.authorizeHttpRequests().requestMatchers("/signUp/**").permitAll();
        httpSecurity.authorizeHttpRequests().requestMatchers("/confirmUser/**").permitAll();
        httpSecurity.authorizeHttpRequests().requestMatchers("/confirmUser").permitAll();

        httpSecurity.authorizeHttpRequests().requestMatchers("/users/**").authenticated();
        httpSecurity.authorizeHttpRequests().requestMatchers("/profile/**").authenticated();
        httpSecurity.authorizeHttpRequests().requestMatchers("/games/**").authenticated();
        httpSecurity.authorizeHttpRequests().requestMatchers("/publishers/**").authenticated();
        httpSecurity.authorizeHttpRequests().requestMatchers("/").authenticated();
        httpSecurity.authorizeHttpRequests().requestMatchers("/age/**").authenticated();

        return httpSecurity.build();
    }

    @Autowired
    public void bindUserDetailsService(AuthenticationManagerBuilder builder,
                                       PasswordEncoder passwordEncoder) throws Exception {
        builder.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Bean
    public PersistentTokenRepository tokenRepository(DataSource dataSource) {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }
}
