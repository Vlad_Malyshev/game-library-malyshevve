package ru.malyshev.gamelibrary.controllers.confirm;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.malyshev.gamelibrary.dto.ConfirmUserForm;
import ru.malyshev.gamelibrary.dto.UserForm;
import ru.malyshev.gamelibrary.services.EmailService;
import ru.malyshev.gamelibrary.services.UsersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/confirmUser")
public class ConfirmUserController {

    private final UsersService usersService;

    private final EmailService emailService;

    @GetMapping("/{user-email}")
    public String sendMailAndGetCode(@PathVariable("user-email") String userEmail) {
        String confirmCodeEncoded = emailService.sendConfirmMessage(userEmail);
        return "redirect:/confirmUser/{user-email}/" + confirmCodeEncoded;
    }

    @GetMapping("/{user-email}/{confirm-code-encoded}")
    public String getConfirmUserPage(@PathVariable("user-email") String userEmail,
                                     @PathVariable("confirm-code-encoded") String confirmCodeEncoded,
                                     Model model) {
        model.addAttribute("userEmail", userEmail);
        model.addAttribute("confirmCodeEncoded", confirmCodeEncoded);
        return "confirm/confirmUserByEmail";
    }

    @GetMapping("/age/{user-email}")
    public String confirmUserAge(@PathVariable("user-email") String email,
                                 Model model) {
        model.addAttribute("userEmail", email);
        return "confirm/confirmUserByAge";
    }

    @PostMapping("/age/{user-email}")
    public String confirmUserAge(@PathVariable("user-email") String userEmail,
                                 UserForm userForm) {
        usersService.confirmUserByAge(userEmail, userForm);
        return "redirect:/games";
    }

    @PostMapping("/{user-email}/{confirm-code-encoded}")
    public String confirmUser(@PathVariable("user-email") String userEmail,
                              @PathVariable("confirm-code-encoded") String confirmCodeEncoded,
                              ConfirmUserForm confirmUserForm) {
        if (usersService.confirmUserByEmail(userEmail, confirmCodeEncoded, confirmUserForm)) {
            return "redirect:/signIn";
        } else {
            return "redirect:/confirmUser/{user-email}/{confirm-code-encoded}?error";
        }
    }
}
