package ru.malyshev.gamelibrary.controllers.sign;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.malyshev.gamelibrary.dto.UserForm;
import ru.malyshev.gamelibrary.services.SignUpService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/";
        }
        return "sign/signUp";
    }

    @PostMapping
    public String signUpUser(UserForm userForm) {
        String userEmail = signUpService.signUp(userForm);
        return "redirect:/confirmUser/" + userEmail;
    }
}
