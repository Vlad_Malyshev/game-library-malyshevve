package ru.malyshev.gamelibrary.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.malyshev.gamelibrary.dto.PublisherForm;
import ru.malyshev.gamelibrary.models.Publisher;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.security.details.CustomUserDetails;
import ru.malyshev.gamelibrary.services.GamesService;
import ru.malyshev.gamelibrary.services.ProfileService;
import ru.malyshev.gamelibrary.services.PublishersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/publishers")
public class PublishersController {

    private final PublishersService publishersService;

    private final ProfileService profileService;

    private final GamesService gamesService;

    @GetMapping
    public String getPublishersPage(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                    Model model) {
        if (profileService.getCurrent(customUserDetails).getState().equals(User.State.NOT_CONFIRMED)) {
            return "redirect:/confirmUser/" + customUserDetails.getUser().getEmail();
        }
        if (profileService.getCurrent(customUserDetails).getAge().equals(0)) {
            return "redirect:/confirmUser/age/" + customUserDetails.getUser().getEmail();
        }
        model.addAttribute("role", profileService.getCurrent(customUserDetails).getRole());
        model.addAttribute("publishersForAdmin", publishersService.getAllPublishers());
        model.addAttribute("publishersForUser", publishersService.getAllReadyPublishers());
        return "publisher/publishers";
    }

    @GetMapping("/{publisher-id}")
    public String getPublisherPage(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                   @PathVariable("publisher-id") Long publisherId,
                                   Model model) {
        if (profileService.getCurrent(customUserDetails).getState().equals(User.State.NOT_CONFIRMED)) {
            return "redirect:/confirmUser/" + customUserDetails.getUser().getEmail();
        }
        if (profileService.getCurrent(customUserDetails).getAge().equals(0)) {
            return "redirect:/confirmUser/age/" + customUserDetails.getUser().getEmail();
        }
        if (publishersService.getPublisher(publisherId).getState().equals(Publisher.State.NOT_READY) && !profileService.getCurrent(customUserDetails).getRole().equals(User.Role.ADMIN)) {
            return "redirect:/publishers";
        }
        model.addAttribute("role", profileService.getCurrent(customUserDetails).getRole());
        model.addAttribute("publisher", publishersService.getPublisher(publisherId));
        model.addAttribute("gamesCreatedByPublisherForUser", gamesService.getAllByPublisherReadyAndAgeLimitLessThanAgeUserGames(profileService.getCurrent(customUserDetails).getAge(), publisherId));
        model.addAttribute("gamesCreatedByPublisherForAdmin", gamesService.getAllGamesByPublisher(publisherId));
        return "publisher/publisher";
    }

    @GetMapping("/{publisher-id}/delete")
    public String deletePublisher(@PathVariable("publisher-id") Long publisherId) {
        publishersService.deletePublisher(publisherId);
        return "redirect:/publishers";
    }

    @GetMapping("/{publisher-id}/prepare")
    public String preparePublisher(@PathVariable("publisher-id") Long publisherId) {
        publishersService.preparePublisher(publisherId);
        return "redirect:/publishers/{publisher-id}";
    }

    @GetMapping("/{publisher-id}/unprepare")
    public String unpreparePublisher(@PathVariable("publisher-id") Long publisherId) {
        publishersService.unpreparePublisher(publisherId);
        return "redirect:/publishers/{publisher-id}";
    }

    @PostMapping("/{publisher-id}/update")
    public String updatePublisher(@PathVariable("publisher-id") Long publisherId,
                                  PublisherForm publisher) {
        publishersService.updatePublisher(publisherId, publisher);
        return "redirect:/publishers/{publisher-id}";
    }

    @PostMapping
    public String addPublisher(PublisherForm publisher) {
        publishersService.addPublisher(publisher);
        return "redirect:/publishers";
    }
}
