package ru.malyshev.gamelibrary.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.malyshev.gamelibrary.dto.CommentForm;
import ru.malyshev.gamelibrary.dto.GameForm;
import ru.malyshev.gamelibrary.models.Game;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.security.details.CustomUserDetails;
import ru.malyshev.gamelibrary.services.CommentService;
import ru.malyshev.gamelibrary.services.GamesService;
import ru.malyshev.gamelibrary.services.ProfileService;
import ru.malyshev.gamelibrary.services.PublishersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/games")
public class GamesController {

    private final GamesService gamesService;

    private final ProfileService profileService;

    private final PublishersService publishersService;

    private final CommentService commentService;

    @GetMapping
    public String getGamesPage(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                               Model model) {
        if (profileService.getCurrent(customUserDetails).getState().equals(User.State.NOT_CONFIRMED)) {
            return "redirect:/confirmUser/" + customUserDetails.getUser().getEmail();
        }
        if (profileService.getCurrent(customUserDetails).getAge().equals(0)) {
            return "redirect:/confirmUser/age/" + customUserDetails.getUser().getEmail();
        }
        model.addAttribute("role", profileService.getCurrent(customUserDetails).getRole());
        model.addAttribute("gamesForAdmin", gamesService.getAllGames());
        model.addAttribute("gamesForUser", gamesService.getAllReadyAndAgeLimitLessThanAgeUserGames(profileService.getCurrent(customUserDetails).getAge()));
        return "game/games";
    }

    @GetMapping("/{game-id}")
    public String getGamePage(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                              @PathVariable("game-id") Long gameId,
                              Model model) {
        if (profileService.getCurrent(customUserDetails).getState().equals(User.State.NOT_CONFIRMED)) {
            return "redirect:/confirmUser/" + customUserDetails.getUser().getEmail();
        }
        if (profileService.getCurrent(customUserDetails).getAge().equals(0)) {
            return "redirect:/confirmUser/age/" + customUserDetails.getUser().getEmail();
        }
        if (gamesService.getGame(gameId).getState().equals(Game.State.NOT_READY) && !profileService.getCurrent(customUserDetails).getRole().equals(User.Role.ADMIN)) {
            return "redirect:/games";
        }
        model.addAttribute("role", profileService.getCurrent(customUserDetails).getRole());
        model.addAttribute("currentUserId", customUserDetails.getUser().getId());
        model.addAttribute("game", gamesService.getGame(gameId));
        model.addAttribute("publishers", publishersService.getAllPublishers());
        model.addAttribute("usersWhoLikeIt", gamesService.getAllUsersWhoLikeIt(gameId));
        model.addAttribute("comments", commentService.getAllCommentToGame(gameId));
        return "game/game";
    }

    @GetMapping("/find/{title-like}")
    public String getGamesByTitleLike(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                      @PathVariable("title-like") String titleLike,
                                      Model model) {
        if (profileService.getCurrent(customUserDetails).getState().equals(User.State.NOT_CONFIRMED)) {
            return "redirect:/confirmUser/" + customUserDetails.getUser().getEmail();
        }
        if (profileService.getCurrent(customUserDetails).getAge().equals(0)) {
            return "redirect:/confirmUser/age/" + customUserDetails.getUser().getEmail();
        }
        model.addAttribute("role", profileService.getCurrent(customUserDetails).getRole());
        model.addAttribute("gamesForAdmin", gamesService.getAllGamesTitleLike(titleLike));
        model.addAttribute("gamesForUser", gamesService.getAllTitleLikeReadyAndAgeLimitLessThanAgeUserGames(profileService.getCurrent(customUserDetails).getAge(), titleLike));
        return "game/games";
    }

    @GetMapping("/{game-id}/delete")
    public String deleteGame(@PathVariable("game-id") Long gameId) {
        gamesService.deleteGame(gameId);
        return "redirect:/games";
    }

    @GetMapping("/{game-id}/prepare")
    public String prepareGame(@PathVariable("game-id") Long gameId) {
        gamesService.prepareGame(gameId);
        return "redirect:/games/{game-id}";
    }

    @GetMapping("/{game-id}/unprepare")
    public String unprepareGame(@PathVariable("game-id") Long gameId) {
        gamesService.unprepareGame(gameId);
        return "redirect:/games/{game-id}";
    }

    @GetMapping("/{game-id}/addFavorite")
    public String addFavoriteGame(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                  @PathVariable("game-id") Long gameId) {
        profileService.addFavoriteGame(customUserDetails, gameId);
        return "redirect:/games/{game-id}";
    }

    @GetMapping("/{game-id}/removeFavorite")
    public String removeFavoriteGame(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                     @PathVariable("game-id") Long gameId) {
        profileService.removeFavoriteGame(customUserDetails, gameId);
        return "redirect:/games/{game-id}";
    }

    @GetMapping("/{game-id}/deleteComment/{comment-id}")
    public String deleteComment(@PathVariable("comment-id") Long commentId) {
        commentService.deleteComment(commentId);
        return "redirect:/games/{game-id}";
    }

    @PostMapping
    public String addGame(GameForm game) {
        gamesService.addGame(game);
        return "redirect:/games";
    }

    @PostMapping("/{game-id}/addPhoto")
    public String addPhoto(@PathVariable("game-id") Long gameId,
                           GameForm game) {
        gamesService.addPhoto(gameId, game);
        return "redirect:/games/{game-id}";
    }

    @PostMapping("/{game-id}/addVideo")
    public String addVideo(@PathVariable("game-id") Long gameId,
                           GameForm game) {
        gamesService.addVideo(gameId, game);
        return "redirect:/games/{game-id}";
    }

    @PostMapping("/{game-id}/update")
    public String updateGame(@PathVariable("game-id") Long gameId,
                             GameForm game) {
        gamesService.updateGame(gameId, game);
        return "redirect:/games/{game-id}";
    }

    @PostMapping("/{game-id}/publisher")
    public String addPublisherToGame(@PathVariable("game-id") Long gameId,
                                     @RequestParam("publisher-id") Long publisherId) {
        gamesService.addPublisherToGame(publisherId, gameId);
        return "redirect:/games/{game-id}";
    }

    @PostMapping("/findGame")
    public String redirectToGamesByTitleLike(String titleLike) {
        if (titleLike.equals("")) {
            return "redirect:/games";
        }
        return "redirect:/games/find/" + titleLike;
    }

    @PostMapping("/{game-id}/addComment")
    public String addCommentToGame(@PathVariable("game-id") Long gameId,
                                   @AuthenticationPrincipal CustomUserDetails customUserDetails,
                                   CommentForm commentForm) {
        commentService.addComment(gameId, customUserDetails.getUser(), commentForm);
        return "redirect:/games/{game-id}";
    }
}
