package ru.malyshev.gamelibrary.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.malyshev.gamelibrary.security.details.CustomUserDetails;
import ru.malyshev.gamelibrary.services.ProfileService;

@Controller
@RequiredArgsConstructor
public class ProfileController {

    private final ProfileService profileService;

    @GetMapping("/")
    public String getRoot() {
        return "redirect:/profile";
    }

    @GetMapping("/profile")
    public String getProfilePage(@AuthenticationPrincipal CustomUserDetails userDetails,
                                 Model model) {
        model.addAttribute("user", profileService.getCurrent(userDetails));
        model.addAttribute("favorites", profileService.getFavoritesGames(userDetails));
        return "profile/profile";
    }

    @PostMapping("/profile/changePassword")
    public String changeUserPassword(@AuthenticationPrincipal CustomUserDetails userDetails,
                                     @RequestParam("new-password") String newPassword) {
        profileService.changeUserPassword(userDetails, newPassword);
        return "redirect:/logout";
    }
}
