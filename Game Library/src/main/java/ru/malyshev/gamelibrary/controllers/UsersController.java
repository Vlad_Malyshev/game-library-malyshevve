package ru.malyshev.gamelibrary.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.malyshev.gamelibrary.dto.UserForm;
import ru.malyshev.gamelibrary.models.User;
import ru.malyshev.gamelibrary.security.details.CustomUserDetails;
import ru.malyshev.gamelibrary.services.CommentService;
import ru.malyshev.gamelibrary.services.ProfileService;
import ru.malyshev.gamelibrary.services.SignUpService;
import ru.malyshev.gamelibrary.services.UsersService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;

    private final ProfileService profileService;

    private final SignUpService signUpService;

    private final CommentService commentService;

    @GetMapping
    public String getUsersPage(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                               Model model) {
        if (!profileService.getCurrent(customUserDetails).getRole().equals(User.Role.ADMIN)) {
            return "redirect:/games";
        }
        model.addAttribute("role", profileService.getCurrent(customUserDetails).getRole());
        model.addAttribute("usersForAdmin", usersService.getAllUsers());
        model.addAttribute("usersForUser", usersService.getAllReadyUsers());
        return "user/users";
    }

    @GetMapping("/{user-id}")
    public String getUserPage(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                              @PathVariable("user-id") Long userId,
                              Model model) {
        if (!profileService.getCurrent(customUserDetails).getRole().equals(User.Role.ADMIN) && !profileService.getCurrent(customUserDetails).getId().equals(userId)) {
            return "redirect:/games";
        }
        model.addAttribute("role", profileService.getCurrent(customUserDetails).getRole());
        model.addAttribute("id", profileService.getCurrent(customUserDetails).getId());
        model.addAttribute("user", usersService.getUser(userId));
        model.addAttribute("commentsByUser", commentService.getAllCommentByUser(userId));
        return "user/user";
    }

    @GetMapping("/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Long userId) {
        usersService.deleteUser(userId);
        return "redirect:/users";
    }

    @GetMapping("/{user-id}/giveRoleAdmin")
    public String giveRoleAdmin(@PathVariable("user-id") Long userId) {
        usersService.giveRoleAdmin(userId);
        return "redirect:/users/{user-id}";
    }

    @GetMapping("/{user-id}/deleteComment/{comment-id}")
    public String deleteComment(@PathVariable("comment-id") Long commentId) {
        commentService.deleteComment(commentId);
        return "redirect:/users/{user-id}";
    }

    @PostMapping
    public String addUser(UserForm user) {
        signUpService.signUp(user);
        return "redirect:/users";
    }

    @PostMapping("/{user-id}/update")
    public String updateUser(@PathVariable("user-id") Long userId,
                             UserForm user) {
        usersService.updateUser(userId, user);
        return "redirect:/users/" + userId;
    }
}
